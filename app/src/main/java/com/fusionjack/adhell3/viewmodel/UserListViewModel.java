package com.fusionjack.adhell3.viewmodel;

import androidx.annotation.NonNull;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;

import com.fusionjack.adhell3.db.repository.BlackListRepository;
import com.fusionjack.adhell3.db.repository.UserListRepository;
import com.fusionjack.adhell3.db.repository.WhiteListRepository;

import java.util.List;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class UserListViewModel extends ViewModel {
    private LiveData<List<String>> items;
    private final UserListRepository repository;

    public UserListViewModel(UserListRepository repository) {
        this.repository = repository;
    }

    public LiveData<List<String>> getItems() {
        if (items == null) {
            items = repository.getItems();
        }
        return items;
    }

    public void addItem(String item, SingleObserver<String> observer) {
        repository.addItem(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public void removeItem(String item, SingleObserver<String> observer) {
        repository.removeItem(item)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(observer);
    }

    public static class BlackListFactory extends ViewModelProvider.NewInstanceFactory {
        @NonNull
        @Override
        @SuppressWarnings("unchecked")
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new UserListViewModel(new BlackListRepository());
        }
    }

    public static class WhiteListFactory extends ViewModelProvider.NewInstanceFactory {
        @NonNull
        @Override
        @SuppressWarnings("unchecked")
        public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
            return (T) new UserListViewModel(new WhiteListRepository());
        }
    }
}

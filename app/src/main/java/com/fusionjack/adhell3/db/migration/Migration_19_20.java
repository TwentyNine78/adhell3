package com.fusionjack.adhell3.db.migration;

import androidx.annotation.NonNull;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

public class Migration_19_20 extends Migration {

    public Migration_19_20(int startVersion, int endVersion) {
        super(startVersion, endVersion);
    }

    @Override
    public void migrate(@NonNull SupportSQLiteDatabase database) {
        database.execSQL("CREATE TABLE FirewallWhitelistedPackage " +
                "(id INTEGER PRIMARY KEY, " +
                "packageName TEXT NOT NULL, " +
                "policyPackageId TEXT DEFAULT 'default-policy', " +
                "FOREIGN KEY (policyPackageId) REFERENCES PolicyPackage(id))");
        database.execSQL("CREATE UNIQUE INDEX firewall_whitelisted_package_policy_package_idx " +
                "ON FirewallWhitelistedPackage (packageName, policyPackageId)");
    }
}

package com.fusionjack.adhell3.fragments;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.widget.PopupMenu;
import androidx.core.view.MenuCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.fusionjack.adhell3.BuildConfig;
import com.fusionjack.adhell3.MainActivity;
import com.fusionjack.adhell3.R;
import com.fusionjack.adhell3.adapter.AppInfoAdapter;
import com.fusionjack.adhell3.databinding.DialogQuestionBinding;
import com.fusionjack.adhell3.databinding.FragmentAppComponentBinding;
import com.fusionjack.adhell3.db.entity.AppInfo;
import com.fusionjack.adhell3.db.repository.AppRepository;
import com.fusionjack.adhell3.model.AppFlag;
import com.fusionjack.adhell3.utils.AdhellFactory;
import com.fusionjack.adhell3.utils.AppComponentFactory;
import com.fusionjack.adhell3.utils.AppPreferences;
import com.fusionjack.adhell3.utils.DialogUtils;
import com.google.android.material.snackbar.Snackbar;

import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers;
import io.reactivex.rxjava3.core.SingleObserver;
import io.reactivex.rxjava3.disposables.Disposable;
import io.reactivex.rxjava3.schedulers.Schedulers;

public class AppComponentFragment extends AppFragment {
    private final boolean showSystemApps = BuildConfig.SHOW_SYSTEM_APP_COMPONENT;
    private FragmentAppComponentBinding binding;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        type = AppRepository.Type.COMPONENT;
        initAppModel(type);

        if (BuildConfig.SHOW_SYSTEM_APP_COMPONENT && !AppPreferences.getInstance().getWarningDialogAppComponentDontShow()) {
            DialogQuestionBinding dialogQuestionBinding = DialogQuestionBinding.inflate(LayoutInflater.from(getContext()));
            dialogQuestionBinding.questionDontShow.setVisibility(View.VISIBLE);
            dialogQuestionBinding.titleTextView.setText(R.string.dialog_system_app_components_title);
            dialogQuestionBinding.questionTextView.setText(R.string.dialog_system_app_components_info);
            AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                    .setView(dialogQuestionBinding.getRoot())
                    .setPositiveButton(android.R.string.yes, ((dialog, which) -> {
                        if (dialogQuestionBinding.questionDontShow.isChecked()) {
                            AppPreferences.getInstance().setWarningDialogAppComponentDontShow(true);
                        }
                    }))
                    .create();

            alertDialog.show();
        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        loadAppList(type);
    }

    @Override
    public void onCreateOptionsMenu(@NonNull Menu menu, @NonNull MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.appcomponent_tab_menu, menu);

        boolean appComponentEnabled = AppPreferences.getInstance().isAppComponentToggleEnabled();
        menu.findItem(R.id.action_enable_all).setEnabled(appComponentEnabled);
        menu.findItem(R.id.action_batch).setEnabled(appComponentEnabled);
        menu.findItem(R.id.action_show_disabled).setEnabled(appComponentEnabled);

        initSearchView(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.action_enable_all) {
            enableAllAppComponents();
        } else if (id == R.id.action_batch) {
            batchOperation();
        } else if (id == R.id.action_show_disabled) {
            if (getParentFragment() != null) {
                AdhellFactory.getInstance().showAppComponentDisabledFragment(getParentFragment().getParentFragmentManager());
            }
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        setHasOptionsMenu(true);

        binding = FragmentAppComponentBinding.inflate(inflater);
        appFlag = AppFlag.createComponentFlag();

        binding.componentAppsList.setAdapter(adapter);

        binding.componentAppsList.setOnItemClickListener((AdapterView<?> adView, View view2, int position, long id) -> {
            AppInfoAdapter adapter = (AppInfoAdapter) adView.getAdapter();
            FragmentManager fragmentManager = null;
            if (getParentFragment() != null) {
                fragmentManager = getParentFragment().getParentFragmentManager();

                Bundle bundle = new Bundle();
                AppInfo appInfo = adapter.getItem(position);
                bundle.putString("packageName", appInfo.packageName);
                bundle.putString("appName", appInfo.appName);
                ComponentTabFragment fragment = new ComponentTabFragment();
                fragment.setArguments(bundle);

                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.replace(R.id.fragmentContainer, fragment);
                fragmentTransaction.addToBackStack("appComponents");
                fragmentTransaction.commit();
            }
        });

        binding.filterButton.setOnClickListener(v -> {
            PopupMenu popup = new PopupMenu(context, binding.filterButton);
            popup.getMenuInflater().inflate(R.menu.filter_appinfo_menu, popup.getMenu());

            if (viewModel.getFilterAppInfo().getValue() != null) {
                popup.getMenu().findItem(R.id.highlightRunningApps).setChecked(viewModel.getFilterAppInfo().getValue().getHighlightRunningApps());
                if (showSystemApps) {
                    popup.getMenu().findItem(R.id.filterSystemApps).setEnabled(true);
                    popup.getMenu().findItem(R.id.filterSystemApps).setChecked(viewModel.getFilterAppInfo().getValue().getSystemAppsFilter());
                } else {
                    popup.getMenu().findItem(R.id.filterSystemApps).setEnabled(false);
                    popup.getMenu().findItem(R.id.filterSystemApps).setChecked(false);
                }
                popup.getMenu().findItem(R.id.filterUserApps).setChecked(viewModel.getFilterAppInfo().getValue().getUserAppsFilter());
                popup.getMenu().findItem(R.id.filterRunningApps).setChecked(viewModel.getFilterAppInfo().getValue().getRunningAppsFilter());
                popup.getMenu().findItem(R.id.filterStoppedApps).setChecked(viewModel.getFilterAppInfo().getValue().getStoppedAppsFilter());
            }

            MenuCompat.setGroupDividerEnabled(popup.getMenu(), true);
            popup.setOnMenuItemClickListener(item -> {
                item.setChecked(!item.isChecked());
                int id = item.getItemId();
                if (id == R.id.highlightRunningApps) {
                    setFilterAppHighlightState(item.isChecked());
                } else if (id == R.id.filterSystemApps) {
                    if (showSystemApps) {
                        setFilterAppSystemState(item.isChecked());
                        if (!item.isChecked()) {
                            if (!popup.getMenu().findItem(R.id.filterUserApps).isChecked()) {
                                popup.getMenu().findItem(R.id.filterUserApps).setChecked(true);
                                setFilterAppUserState(true);
                            }
                        }
                    }
                } else if (id == R.id.filterUserApps) {
                    setFilterAppUserState(item.isChecked());
                    if (!item.isChecked()) {
                        if (!popup.getMenu().findItem(R.id.filterSystemApps).isChecked()) {
                            popup.getMenu().findItem(R.id.filterSystemApps).setChecked(true);
                            setFilterAppSystemState(true);
                        }
                    }
                } else if (id == R.id.filterRunningApps) {
                    setFilterAppRunningState(item.isChecked());
                    if (!item.isChecked()) {
                        if (!popup.getMenu().findItem(R.id.filterStoppedApps).isChecked()) {
                            popup.getMenu().findItem(R.id.filterStoppedApps).setChecked(true);
                        }
                    }
                } else if (id == R.id.filterStoppedApps) {
                    setFilterAppStoppedState(item.isChecked());
                    if (!item.isChecked()) {
                        if (!popup.getMenu().findItem(R.id.filterRunningApps).isChecked()) {
                            popup.getMenu().findItem(R.id.filterRunningApps).setChecked(true);
                            setFilterAppRunningState(true);
                        }
                    }
                }
                return false;
            });
            popup.show();
        });

        rootView = binding.getRoot();
        super.onCreateView(inflater, container, savedInstanceState);

        return binding.getRoot();
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        binding = null;
        rootView = null;
    }

    private void batchOperation() {
        DialogQuestionBinding dialogQuestionBinding = DialogQuestionBinding.inflate(LayoutInflater.from(getContext()));
        dialogQuestionBinding.titleTextView.setText(R.string.dialog_appcomponent_batch_title);
        dialogQuestionBinding.questionTextView.setText(R.string.dialog_appcomponent_batch_summary);

        AlertDialog progressDialog = DialogUtils.getProgressDialog("", context);
        progressDialog.setCancelable(false);

        SingleObserver<String> observer = new SingleObserver<String>() {
            @Override
            public void onSubscribe(@NonNull Disposable d) {
            }

            @Override
            public void onSuccess(@NonNull String s) {
                progressDialog.dismiss();
                if (getActivity() instanceof MainActivity) {
                    MainActivity mainActivity = (MainActivity) getActivity();
                    mainActivity.makeSnackbar(s, Snackbar.LENGTH_LONG)
                            .show();
                }
            }

            @Override
            public void onError(Throwable e) {
                progressDialog.dismiss();
                if (e.getMessage() != null) {
                    if (getActivity() instanceof MainActivity) {
                        MainActivity mainActivity = (MainActivity) getActivity();
                        mainActivity.makeSnackbar(e.getMessage(), Snackbar.LENGTH_LONG)
                                .show();
                    }
                }
            }
        };

        AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setView(dialogQuestionBinding.getRoot())
                .setPositiveButton(R.string.button_enable, (dialog, whichButton) -> {
                    progressDialog.show();
                    DialogUtils.setProgressDialogMessage(progressDialog, getString(R.string.dialog_appcomponent_enable_summary));
                    AppComponentFactory.getInstance().processAppComponentInBatch(true)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(observer);
                })
                .setNegativeButton(R.string.button_disable, (dialog, whichButton) -> {
                    progressDialog.show();
                    DialogUtils.setProgressDialogMessage(progressDialog, getString(R.string.dialog_appcomponent_disable_summary));
                    AppComponentFactory.getInstance().processAppComponentInBatch(false)
                            .subscribeOn(Schedulers.io())
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe(observer);
                })
                .setNeutralButton(android.R.string.no, null)
                .create();

        alertDialog.show();

        if (getView() != null) {
            AppComponentFactory.getInstance().checkMigrateOldBatchFiles(getContext());
        }
    }

    private void enableAllAppComponents() {
        DialogQuestionBinding dialogQuestionBinding = DialogQuestionBinding.inflate(LayoutInflater.from(getContext()));
        dialogQuestionBinding.titleTextView.setText(R.string.dialog_enable_components_title);
        dialogQuestionBinding.questionTextView.setText(R.string.dialog_enable_components_info);
        AlertDialog alertDialog = new AlertDialog.Builder(context, R.style.AlertDialogStyle)
                .setView(dialogQuestionBinding.getRoot())
                .setPositiveButton(android.R.string.yes, (dialog, whichButton) ->
                        AsyncTask.execute(() -> {
                            AdhellFactory.getInstance().setAppComponentState(true);
                            AdhellFactory.getInstance().getAppDatabase().appPermissionDao().deleteAll();
                        })
                )
                .setNegativeButton(android.R.string.no, null)
                .create();

        alertDialog.show();
    }
}

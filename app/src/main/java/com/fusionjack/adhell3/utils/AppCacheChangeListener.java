package com.fusionjack.adhell3.utils;

public interface AppCacheChangeListener {
    public void onAppCacheChange();
}
